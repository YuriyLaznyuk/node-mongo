const express = require('express');
const app = express();
const path = require('path');
let PORT = process.env.PORT || 3030;
const MongoClient = require("mongodb").MongoClient;
const objectId = require('mongodb').ObjectId;
const jsonParser = express.json();
// const rootDir = path.join(path.resolve(), 'dist');
const rootDir = path.resolve('dist');
require('dotenv').config();
let dbClient;

// const url = "mongodb://localhost:27017/";
const mongoClient = new MongoClient(process.env.DB, {useUnifiedTopology: true});

//connect
mongoClient.connect((err, client) => {
    if (err) {
        return console.log(err);
    }
    // app.locals.collection = client.db('usersdb').collection('users');
    app.locals.collection = client.db('new_base').collection('users');

});
// app.use(express.static(rootDir + "/"));
app.use(express.static(path.join(rootDir,"/")));

app.get("/", (req, res) => {
    // res.sendFile(rootDir + "/index.html");
    res.sendFile(path.join(rootDir,'index.html'));
});
//GET
app.get('/api/result', (req, res) => {
    const collection = req.app.locals.collection;
    collection.find().toArray((err, result) => {
        if (err) {
            console.log(err);
        }
        res.send(result);
    });
});
//GET id
app.get('/api/result/:id', (req, res) => {
    const collection = req.app.locals.collection;
    // const id=req.params.id;
    const id = new objectId(req.params.id);

    collection.findOne({_id: id}, (err, result) => {
        if (err) {
            return console.log(err);
        }
        res.send(result);
    });

});

//POST//
app.post('/api/result', jsonParser, (req, res) => {
    if (!req.body) {
        return res.sendStatus(404);
    }

    const collection = req.app.locals.collection;
    const reqName = req.body.name;
    const reqAge = req.body.age;

    const user = {name: reqName, age: reqAge};
    collection.insertOne(user, (err, result) => {
        if (err) {
            return console.log(err);
        }
        res.send(result);
    });

});

//PUT//

app.put("/api/result", jsonParser, (req, res) => {
    if (!req.body) {
        return res.sendStatus(400);
    }
    const collection = req.app.locals.collection;
    const reqName = req.body.name;
    const reqAge = req.body.age;
    const id = new objectId(req.body.id);

    collection.findOneAndUpdate({_id: id}, {$set: {name: reqName, age: reqAge}},
        {returnOriginal: false},
        (err, result) => {
            if (err) {
                return console.log(err);
            }
            const user = result.value;
            res.send(user);
        });
});

//DELETE//
app.delete("/api/result/:id", (req, res) => {
    const id = new objectId(req.params.id);
    const collection = req.app.locals.collection;
    collection.findOneAndDelete({_id: id}, function (err, result) {
        if (err) {
            return console.log(err);
        }
        res.send(result.value);
    });
});

// "SIGINT", которое генерируется при нажатии комбинации CTRL+C
process.on('SIGINT', () => {
    dbClient.close();
    process.exit();
});

app.listen(PORT, () => console.log(`server PORT ${PORT}`));
