import React, {useState, useEffect} from 'react';
import './style/mainPage.scss';

function MainPage(props) {
    const location = window.location.origin;
    const [date, setDate] = useState({users: [], name: '', age: '', status: false, edit: false, id: false});
    useEffect(() => {
        fetch(location + '/api/result')
            .then(res => res.json())
            .then(json => setDate({...date, users: json}))
            .catch(err => console.log(err));
    }, [date.status]);

    function getEdit(id, name, age) {
        setDate({...date, name: name, age: age, edit: true, id: id});
    }

    function getAdd(name, age, edit, id) {
        if (!name) return false;
        if (!age) return false;

        if (edit) {
            fetch(location + `/api/result`, {
                method: 'PUT',
                headers: {"Content-Type": "application/json; charset=utf-8"},
                body: JSON.stringify({
                    name: name,
                    age: age,
                    id: id
                })
            }).then(res => res.json())
                .then(json => {
                    setDate({...date, name: '', age: '', edit: false, id: false, status:!date.status});
                }).catch(err => console.log(err));

        } else

            fetch(location + `/api/result`, {
                method: "POST",
                headers: {"Content-Type": "application/json; charset=utf-8"},
                body: JSON.stringify({
                    name: name,
                    age: age
                })
            }).then(res => res.json())
                .then(json => {
                    setDate({...date, status: !date.status, name: "", age: ""});
                    console.log(json);
                }).catch(err => console.log(err));
    }

    function getDelete(id) {
        fetch(location + `/api/result/${id}`,
            {
                method: "DELETE",
                headers: {"Content-Type": "application/json; charset=utf-8"}
            }).then(res => res.json())
            .then(json => {
                setDate({...date, status: !date.status});
                console.log(json);
            })
            .catch(err => console.log(err));
    }

    function getRow() {
        return (
            (!date.users.length>0) ? "db null" :
            date.users.map(item => (
                <tr key={item._id}>
                    <td>{item.name}</td>
                    <td>{item.age}</td>
                    <td>
                        <button onClick={() => getEdit(item._id, item.name, item.age)}>edit</button>
                        <button onClick={() => getDelete(item._id)}>delete</button>
                    </td>
                </tr>
            ))
        );

    }

    return (
        <div className="mainPage">
            <table className='mainPage__table'>
                <thead>
                <tr>
                    <th><input onChange={e => setDate({...date, name: e.target.value})}
                               value={date.name} type="text" placeholder='add name'/></th>
                    <th><input onChange={e => setDate({...date, age: e.target.value})}
                               value={date.age} type="text" placeholder='add age'/></th>
                    <th>
                        <button onClick={() => getAdd(date.name, date.age, date.edit, date.id)}>{date.edit ? 'edit user' : 'add user'}</button>
                        <button onClick={()=>setDate({...date, name: '', age: '', edit: false, id: false})}>cancel</button>
                    </th>
                </tr>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Control</th>
                </tr>
                </thead>
                <tbody>
                {getRow()}
                </tbody>
            </table>
        </div>
    );
}

export default MainPage;