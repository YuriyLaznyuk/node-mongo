import React from 'react';
import "./app.scss";
import MainPage from "./public/MainPage";

function App(props) {
    return (
        <div>
            <h1>MongoDb REST API</h1>
            <MainPage/>
        </div>
    );
}

export default App;